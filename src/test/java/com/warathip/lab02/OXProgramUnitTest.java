/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.warathip.lab02;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author win10
 */
public class OXProgramUnitTest {

    public OXProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_X_output_false() {
        String[][] board = {{"-", "-", "-",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String player = "X";
        assertEquals(false, OXProgram.checkWin(board,player));
    }
       @Test
    public void testCheckWinNoPlayBY_O_false() {
        String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        String player = "O";
        assertEquals(false, OXProgram.checkWin(board, player));
    }
     @Test
    public void testCheckWinRow1BY_X_output_true() {
        String[][] board = {{"X", "X", "X",}, {"-", "-", "-",}, {"-", "-", "-",}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board,player));
    }
    @Test
    public void testCheckWinRow2PlayBY_X_true() {
        String[][] board = {{"_", "_", "_"},{"X", "X", "X"},{"_", "_", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    @Test
    public void testCheckWinRow3PlayBY_X_true() {
        String[][] board = {{"_", "_", "_"},{"_", "_", "_"},{"X", "X", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
     @Test
    public void testCheckWinRow1PlayBY_O_true() {
        String[][] board = {{"O", "O", "O"}, {"_", "_", "_"}, {"_", "_", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
      @Test
    public void testCheckWinRow2PlayBY_O_true() {
        String[][] board = {{"_", "_", "_"},{"O", "O", "O"},{"_", "_", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
      @Test
    public void testCheckWinRow3PlayBY_O_true() {
        String[][] board = {{"_", "_", "_"},{"_", "_", "_"},{"O", "O", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
      @Test
    public void testCheckWinCol1PlayBY_X_true() {
        String[][] board = {{"X", "_", "_"},{"X", "_", "_"},{"X", "_", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
     @Test
    public void testCheckWinCol2PlayBY_X_true() {
        String[][] board = {{"_", "X", "_"},{"_", "X", "_"},{"_", "X", "_"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
      @Test
    public void testCheckWinCol3PlayBY_X_true() {
        String[][] board = {{"_", "_", "X"},{"_", "_", "X"},{"_", "_", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    @Test
    public void testCheckWinCol1PlayBY_O_true() {
        String[][] board = {{"O", "_", "_"},{"O", "_", "_"},{"O", "_", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    @Test
    public void testCheckWinCol2PlayBY_O_true() {
        String[][] board = {{"_", "O", "_"},{"_", "O", "_"},{"_", "O", "_"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
    @Test
    public void testCheckWinCol3PlayBY_O_true() {
        String[][] board = {{"X", "X", "O"},{"_", "_", "O"},{"_", "_", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
     @Test
    public void testCheckWinX1PlayBY_X_true() {
        String[][] board = {{"X", "O", "O"},{"O", "X", "O"},{"O", "O", "X"}};
        String player = "X";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
      @Test
    public void testCheckWinX2PlayBY_O_true() {
        String[][] board = {{"X", "X", "O"},{"_", "O", "_"},{"O", "O", "X"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
       @Test
    public void testCheckDrawPlayBY_X_true() {
        String[][] board = {{"X", "O", "O"},{"O", "X", "X"},{"X", "X", "O"}};
        String player = "O";
        assertEquals(true, OXProgram.checkWin(board, player));
    }
}
